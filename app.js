new Vue({
    el: "#app",
    data: {
        formData: {
            fName: '',
        },
        aValue: 0,
        bValue: 0,
        cValue: 0,
        salary: 10,
        name: "ratul", //string value
        age: 27,
        designation: "software engineer",
        fruits: ['apple', 'mango', 'banana'], //array
        //create object type 
        batch: {
            student: 20,
            group: 'science',
        },
        students: {
            name: "shourov",
            address: "dhaka",
            mobile: "123",
            batch: "08"
        },
        person: {
            fName: '',
            lName: ''
        },
        src: "https://picsum.photos/seed/picsum/200/300",
        link: "http://google.com",
        htmlTag: "<b>welcome </b>",
        x: 0,
        y: 0,
        message: () => { //create function using ES6
            return "welcome to vue js learning"
        }
    },
    computed: {
        aMethod() {
            console.log("aMethod");
            return this.salary + this.aValue;

        },
        bMethod() {
            console.log("bMethod");
            return this.salary + this.bValue;

        },
        cMethod() {
            console.log("cMethod");
            return this.salary + this.cValue;

        },
    },
    methods: {
        // aMethod() {
        //     console.log("aMethod");
        //     return this.salary + this.a;

        // },
        // bMethod() {
        //     console.log("bMethod");
        //     return this.salary + this.b;

        // },
        // cMethod() {
        //     console.log("cMethod");
        //     return this.salary + this.c;

        // },
        onSubmit() {
            console.log(this.formData.fName);
        },
        testingEvent(message, event) {
            console.log(event);
            alert("hello", message);
        },
        //normal way to create funtion
        firstName: function() {
            return this.name;
        },
        //another way to create 
        info() {
            return this.designation;
        },
        updateName() {
            setTimeout(() => {
                this.name = "shourov";
            }, 2000);
        },
        changeDesignation() {
            this.designation = "Ict Euro Ltd Software Engineer ";
        },
        changeCoordinate(event) {
            this.x = event.clientX;
            this.y = event.clientY;
        },
        formSubmit(event) {
            event.preventDefault();
            console.log(this.person);
        }
    },
});
new Vue({
    el: "#app2",
    data: {
        a: 0,
    },
    methods: {
        increment() {
            console.log();
            this.a += 5;
        }
    }
});